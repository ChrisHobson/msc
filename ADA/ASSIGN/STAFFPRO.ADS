-------------------------------------------------------------------------------
-- Specification for the command processing package
--
-- The command processing package is given a valid menu selection
-- and performs the required action. It only has two public methods:-
--
-- SetSortKey
-- -----------
-- This should be called at program startup to inform the package
-- which key the printout is to be sorted on.
--
-- ProcessCommand
-- --------------
-- This is passed a MainCommand type and a Staff_Number upon which the
-- command is to act. The Staff_Number is only used by the Edit and Delete
-- commands.
--
-- ----------------------------------------------------------------------------
-- Chris Hobson : MSc Software Development (28001000944674)
-- ----------------------------------------------------------------------------

With StaffEntry; USE StaffEntry;
with Menu; USE Menu;

package StaffProcess is

procedure SetSortKey(key: StaffKey);
procedure ProcessCommand(command: MainCommand; id: Staff_Num);

private
function DoCreate(staff: in StaffMemberPtr) return Boolean;
-- Private internal function used to actually create staff entries

end StaffProcess;

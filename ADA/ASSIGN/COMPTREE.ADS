-------------------------------------------------------------------------------
-- Specification of a enumerated type for comparision
--
-- ----------------------------------------------------------------------------
-- Chris Hobson : MSc Software Development (28001000944674)
-- ----------------------------------------------------------------------------
package CompTree is

type CompType is (Less,Greater,Equal);

end CompTree;

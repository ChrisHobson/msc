(* ============================================================================
** <One line description>
**
** <Full description.>
**
** ----------------------------------------------------------------------------
** Chris Hobson : MSc Software Development (28001000944674) Coventry University
** ----------------------------------------------------------------------------
**
** Revision History
** Who When     What   
** --- -------- ---------------------------------------------------------------
** CLH dd/mm/yy Written.
** ----------------------------------------------------------------------------
*)

IMPLEMENTATION MODULE Template;


(* Imports from CLH's modules *)

(* Imports from system modules *)

(* Constants *)
(* CONST *)

(* Variables *)
(* VAR *)

(* ============================================================================
** <One line description>
**
** <Full description.>
**
** ----------------------------------------------------------------------------
** Chris Hobson : MSc Software Engineering (28001000944674) Coventry University
** ----------------------------------------------------------------------------
**
** Revision History
** Who When     What   
** --- -------- ---------------------------------------------------------------
** CLH dd/mm/yy Written.
** ----------------------------------------------------------------------------
*)

PROCEDURE xyz(val :REAL);

BEGIN

END xyz;

(* ============================================================================
** <One line description>
**
** <Full description.>
**
** ----------------------------------------------------------------------------
** Chris Hobson : MSc Software Engineering (28001000944674) Coventry University
** ----------------------------------------------------------------------------
**
** Revision History
** Who When     What   
** --- -------- ---------------------------------------------------------------
** CLH dd/mm/yy Written.
** ----------------------------------------------------------------------------
*)

PROCEDURE xyz2(val :CARDINAL);

BEGIN

END xyz2;


END Template.

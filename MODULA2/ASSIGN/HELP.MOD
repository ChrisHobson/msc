(* =============================================================== Assignment 3
**  This program prompts for
**    o Upto 10 subject names
**    o Upto 100 student names
**
**    For each student obtain their marks in each subject. Then
**    produce a table showing the entered data + the average mark
**    for each student and the average mark for each subject in a pretty
**    table.
**
**    Student and subject names can NOT have spaces, modify PromptStr if
**    you need this. (Not sure how to do this, you probably have to read
**    one char at a time until an EOL if found
**
**  Note: This program was knocked togther by converting my assignment
**        3 program. It works as far as I can tell but it needs
**        checking, tidying, converting to your own style, and commenting
**        I (of course) accept no responsibility, etc. etc. 
**        i.e DON'T assume the below is a model answer. (bet it is though!)
**
** ----------------------------------------------------------------------------
** Chris Hobson : MSc Software Development (28001000944674)
** ----------------------------------------------------------------------------
** CLH 30/11/96 Written
*)

MODULE Help;

FROM InOut IMPORT WriteString, WriteCard, WriteLn, Write,
                  ReadCard, ReadString, Done;
FROM StdStrings IMPORT Compare, CompareResult, Length;

CONST
  MAXSUBLEN = 6;      (* Maximum subject length *)
  MAXSUBNO  = 10;     (* Maximum No. Subjects *)
  MAXSTDLEN = 20;     (* Maximum student length *)
  MAXSTDNO  = 100;    (* Maximum No. Students *)

TYPE
  SubName = ARRAY[0..MAXSUBLEN - 1] OF CHAR;
  StdName = ARRAY[0..MAXSTDLEN - 1] OF CHAR;

(* The mark array is a 2d array of marks for each subject for each
** student. Notice how the first array subscript is actually one more than
** the number of subjects (i.e 11 elements) so that the total mark for
** a student can be stored in the array as the data is read in. This prevents 
** the need for a special extra loop  to sum up a students marks
*)

  MarkArray   = ARRAY[0..MAXSUBNO],[0..MAXSTDNO - 1] OF CARDINAL;

(* ----------------------------------------------------------------------------
** Very simple procedure to print strings followed by a new line
** --------------------------------------------------------------------------*)

PROCEDURE WriteStringLn(str :ARRAY OF CHAR);

BEGIN

  WriteString(str);
  WriteLn;

END WriteStringLn;

(* ----------------------------------------------------------------------------
** Prompt for a Cardinal value using 2 strings to build up the prompt line
** this function also contains some basic error checking to ensure only a 
** valid cardinal is returned.
** --------------------------------------------------------------------------*)

PROCEDURE Prompt2StrCard(promptOne, promptTwo :ARRAY OF CHAR) :CARDINAL;

VAR
  value :CARDINAL;

BEGIN

  REPEAT
    (* Display the prompt strings *)
    WriteString(promptOne); WriteString(promptTwo); WriteString(' > ');
    (* Read the users input and display an error if ReadCard failed *)
    ReadCard(value);
    IF(NOT Done) THEN
      WriteLn;
      WriteStringLn('Error: Enter a positive whole value');
    END;
    (* Continue prompting until a correct value is read *)
  UNTIL(Done);

  RETURN value;

END Prompt2StrCard;

(* ----------------------------------------------------------------------------
** Formats a string into a field of the size specified, extra spaces are 
** printed at the end of the string if necessary.
** --------------------------------------------------------------------------*)

PROCEDURE FormatString(str :ARRAY OF CHAR; minField :CARDINAL);

VAR
  count      :CARDINAL;  (* A simple loop counter *)

BEGIN

  WriteString(str);

  (* Pad the string with extra spaces *)
  FOR count := Length(str) + 1 TO minField DO
    Write(" ");
  END;

END FormatString;

(* ----------------------------------------------------------------------------
** Prompts for a string using an arguments at the prompt. If the string that
** is entered is 'Q' or 'q' then
** return FALSE indicating that the user has finished. Otherwise return TRUE
** indicating that a valid string has been read.
** --------------------------------------------------------------------------*)

PROCEDURE PromptStr(prompt :ARRAY OF CHAR; VAR name :ARRAY OF CHAR) :BOOLEAN;

BEGIN

  (* Prompt for the string *)
  WriteString(prompt);WriteString(" ('Q' to end) > ");
  ReadString(name);

  (* Return FALSE if the name is 'Q' or 'q' *)
  RETURN ((Compare(name,'Q') <> equal) AND
          (Compare(name,'q') <> equal));

END PromptStr;

(* ----------------------------------------------------------------------------
** Loop to read subjects. Stop when either
** the limit (maxToRead) is reached or when the user indicates the end.
** --------------------------------------------------------------------------*)

PROCEDURE ReadSubjects(VAR subjects : ARRAY OF SubName;
                       VAR numRead :CARDINAL;
                       maxToRead :CARDINAL);
BEGIN
  numRead := 0;

  (* Loop whilst the limit has not been reach and whilst the user
  ** wishes to enter more details (ie. whilst PromptStr returns TRUE *)

  WHILE((numRead < maxToRead) AND (PromptStr("Subject:",subjects[numRead]))) DO
    INC(numRead);
  END;

END ReadSubjects;

(* ----------------------------------------------------------------------------
** Loop to read the information for each student. Stop when either
** the limit (maxToRead) is reached or when the user indicates the end.
** --------------------------------------------------------------------------*)

PROCEDURE ReadStudents(subjects :ARRAY OF SubName;
                       numSub   :CARDINAL;
                       VAR students : ARRAY OF StdName;
                       VAR numRead :CARDINAL;
                       maxToRead :CARDINAL;
                       VAR marks :MarkArray
                      );
VAR 
  count :CARDINAL; (*For loop counter *)
  subTotal :ARRAY[1..MAXSUBNO] OF CARDINAL;
  val :CARDINAL;

BEGIN
  numRead := 0;

  (* Loop whilst the limit has not been reach and whilst the user
  ** wishes to enter more details (ie. whilst PromptStr returns TRUE *)

  WHILE((numRead < maxToRead) AND 
        (PromptStr("Student Name:",students[numRead]))) DO
   (* We have a valid student name, we need to get the marks
   ** in each of the numSub subjects. Set the numSub element to 0 as this
   ** is used to store the total marks for a student *)
   marks[numSub][numRead] := 0;
   FOR count := 0 TO numSub - 1 DO
    (* Read a mark *)
     val := Prompt2StrCard('Enter marks in ',subjects[count]);
     marks[count][numRead] := val;
    INC(marks[numSub][numRead],val);
   END;


    INC(numRead);
  END;

END ReadStudents;


(* ----------------------------------------------------------------------------
** Display all of the data in a neatly formated table
** --------------------------------------------------------------------------*)

PROCEDURE DisplayTable(subjects :ARRAY OF SubName;
                       numSub   :CARDINAL;
                       VAR students : ARRAY OF StdName;
                       numStd :CARDINAL;
                       marks :MarkArray);

VAR
  count :CARDINAL;
  countMark :CARDINAL;
  subTotal :ARRAY[0..MAXSUBNO - 1] OF CARDINAL;
  val :CARDINAL;

BEGIN

  (* Display some blank lines *)
  WriteLn;WriteLn;WriteLn;

  (* Leave space for student name *)
  FormatString("Student",MAXSTDLEN);  
  (* Loop through each subject and display name *)
  FOR count := 0 TO numSub - 1 DO
    (* Initialize it's total to 0 *)
    subTotal[count] := 0;
    WriteString('|');
    FormatString(subjects[count],MAXSUBLEN);
  END;
  WriteStringLn('|Average');

  (* And each student in a separate row + average *)
  FOR count := 0 TO numStd - 1 DO
    FormatString(students[count],MAXSTDLEN);
    FOR countMark := 0 TO numSub - 1 DO
      WriteString('|');
      val := marks[countMark][count];
      WriteCard(val,MAXSUBLEN);
      INC(subTotal[countMark],val);
    END;
    WriteString('|');
    (* NOTE The below should be in floating point but as every compiler
    ** is different I've used cardinal 
    (it's close enough to show the prinicapl *)
    WriteCard(marks[numSub][count] DIV numSub,MAXSUBLEN);
    WriteLn;
  END;

  (* Finally output averages for the subject, again should be reals *)
  WriteLn;
  FormatString('Averages',MAXSTDLEN);
  FOR count := 0 TO numSub - 1 DO
    WriteString('|');
    WriteCard(subTotal[count] DIV numSub,MAXSUBLEN);
  END;
  WriteStringLn('|');

END DisplayTable;


(* ----------------------------------------------------------------------------
** The main program. This follows the standard small program format of
** Read_Input ... Process_Input ... Display_Results
** --------------------------------------------------------------------------*)

CONST
  MAXCOUPLE = 20;

VAR
  subjectNames  :ARRAY [1..MAXSUBNO] OF SubName;
  studentNames  :ARRAY [1..MAXSTDNO] OF StdName;
  numSubject    :CARDINAL;
  numStudent    :CARDINAL;
  marks         :MarkArray;

BEGIN

  (* Read the income information *)
  ReadSubjects(subjectNames,numSubject,MAXSUBNO);
  ReadStudents(subjectNames,numSubject,
               studentNames,numStudent,MAXSTDNO, marks);
  DisplayTable(subjectNames,numSubject,studentNames,numStudent,marks);

END Help.


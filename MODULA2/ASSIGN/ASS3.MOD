(* =============================================================== Assignment 3
**  This program prompts for a series of 
**    o Married couples surnames
**    o their individual incomes
**
** It then displays a table showing these data + the joint income
**
** The program is split into procedures which could be part of a utility
** module :-
**
** WriteStringLn, Prompt2StrCard, FormatString
**
** Followed by those which are specefic to this program:-
**
** PromptName, ReadCouple, ComputeTotal, DisplayTotals
**
** ----------------------------------------------------------------------------
** Chris Hobson : MSc Software Development (28001000944674)
** ----------------------------------------------------------------------------
** CLH 16/11/96 Written
*)

MODULE Ass3;

FROM InOut IMPORT WriteString, WriteCard, WriteLn, Write,
                  ReadCard, ReadString, Done;
FROM StdStrings IMPORT Compare, CompareResult, Length;

CONST
  MAXNAMELEN = 15;      (* Maximum allowed surname length *)

(* CoupleNameType is used to allow an array of names to be defined, note
** that ARRAY OF CHAR should always start at an index of 0 (page 149 in
** Making sense of Modula2) so upper limit is one less than MAXNAMELEN *)

TYPE
  CoupleNameType = ARRAY[0..MAXNAMELEN - 1] OF CHAR;

(* ----------------------------------------------------------------------------
** Very simple procedure to print strings followed by a new line
** --------------------------------------------------------------------------*)

PROCEDURE WriteStringLn(str :ARRAY OF CHAR);

BEGIN

  WriteString(str);
  WriteLn;

END WriteStringLn;

(* ----------------------------------------------------------------------------
** Prompt for a Cardinal value using 2 strings to build up the prompt line
** this function also contains some basic error checking to ensure only a 
** valid cardinal is returned.
** --------------------------------------------------------------------------*)

PROCEDURE Prompt2StrCard(promptOne, promptTwo :ARRAY OF CHAR) :CARDINAL;

VAR
  value :CARDINAL;

BEGIN

  REPEAT
    (* Display the prompt strings *)
    WriteString(promptOne); WriteString(promptTwo); WriteString(' > ');
    (* Read the users input and display an error if ReadCard failed *)
    ReadCard(value);
    IF(NOT Done) THEN
      WriteLn;
      WriteStringLn('Error: Enter a positive whole value');
    END;
    (* Continue prompting until a correct value is read *)
  UNTIL(Done);

  RETURN value;

END Prompt2StrCard;

(* ----------------------------------------------------------------------------
** Formats a string into a field of the size specified, extra spaces are 
** printed at the end of the string if necessary.
** --------------------------------------------------------------------------*)

PROCEDURE FormatString(str :ARRAY OF CHAR; minField :CARDINAL);

VAR
  count      :CARDINAL;  (* A simple loop counter *)

BEGIN

  WriteString(str);

  (* Pad the string with extra spaces *)
  FOR count := Length(str) + 1 TO minField DO
    Write(" ");
  END;

END FormatString;

(* ----------------------------------------------------------------------------
** Requests the surname of a couple. If the name is entered as 'Q' or 'q' then
** return FALSE indicating that the user has finished. Otherwise return TRUE
** indicating that a valid surname has been read.
** --------------------------------------------------------------------------*)

PROCEDURE PromptName(VAR name :CoupleNameType) :BOOLEAN;

BEGIN

  (* Prompt for and read a surname *)
  WriteString("Enter Surname ('Q' to end) > ");
  ReadString(name);

  (* Return FALSE if the name is 'Q' or 'q' *)
  RETURN ((Compare(name,'Q') <> equal) AND
          (Compare(name,'q') <> equal));

END PromptName;

(* ----------------------------------------------------------------------------
** Loop to read the information for a series of couples. Stop when either
** the limit (maxToRead) is reached or when the user indicates the end.
** --------------------------------------------------------------------------*)

PROCEDURE ReadCouples(VAR names : ARRAY OF CoupleNameType;
                      VAR manIn, womIn :ARRAY OF CARDINAL;
                      VAR numRead :CARDINAL;
                      maxToRead :CARDINAL);
BEGIN
  numRead := 0;

  (* Loop whilst the limit has not been reach and whilst the user
  ** wishes to enter more details (ie. whilst PromptName returns TRUE *)

  WHILE((numRead < maxToRead) AND (PromptName(names[numRead]))) DO
    (* Read both the mans and womans income using Prompt2StrCard to
    ** display a sensible gender based prompt *)
    manIn[numRead] := Prompt2StrCard('Income for  Mr. ',names[numRead]);
    womIn[numRead] := Prompt2StrCard('Income for Mrs. ',names[numRead]);

    INC(numRead);
  END;

END ReadCouples;

(* ----------------------------------------------------------------------------
** Sum the mans and womans income into a total income array
** --------------------------------------------------------------------------*)

PROCEDURE ComputeTotals(manIn, womIn :ARRAY OF CARDINAL;
                        VAR totIn :ARRAY OF CARDINAL;
                        numCouple :CARDINAL);
VAR
  count :CARDINAL;

BEGIN

  FOR count := 0 TO numCouple - 1 DO
    totIn[count] := manIn[count] + womIn[count];
  END;

END ComputeTotals;

(* ----------------------------------------------------------------------------
** Display all of the data in a neatly formated table
** --------------------------------------------------------------------------*)

PROCEDURE DisplayTotals(names : ARRAY OF CoupleNameType;
                        manIn, womIn, totIn :ARRAY OF CARDINAL;
                        numCouple :CARDINAL);

CONST
  INCOMEWIDTH = 10;   (* Field width to display income data in *)

VAR
  count :CARDINAL;

BEGIN

  (* Display some blank lines *)
  WriteLn;WriteLn;WriteLn;
  (* 
  ** Print table heading in correct colums based on MAXNAMELEN and 
  ** INCOMEWIDTH the spacing are hard coded but could be automated if
  ** it is likely that they would change. However all the field width info
  ** is localised in this one procedure to allow easy modification should the
  ** need arise
  *)
              (*1234567890123456789012345678901234567890123456789*)
  WriteStringLn('                     MALE      FEMALE    TOTAL');
  WriteStringLn('NAME                 INCOME    INCOME    INCOME');

  (* Loop through each couple and output their data *)
  FOR count := 0 TO numCouple - 1 DO
    FormatString(names[count],MAXNAMELEN+1);
    WriteCard(manIn[count],INCOMEWIDTH);
    WriteCard(womIn[count],INCOMEWIDTH);
    WriteCard(totIn[count],INCOMEWIDTH);
    WriteLn;
  END;

END DisplayTotals;

(* ----------------------------------------------------------------------------
** The main program. This follows the standard small program format of
** Read_Input ... Process_Input ... Display_Results
** with the except that results are only displayed if any data is entered
** otherwise a message is displayed.
** --------------------------------------------------------------------------*)

CONST
  MAXCOUPLE = 20;

VAR
  maleIncome   :ARRAY [1..MAXCOUPLE] OF CARDINAL;
  femaleIncome :ARRAY [1..MAXCOUPLE] OF CARDINAL;
  totalIncome  :ARRAY [1..MAXCOUPLE] OF CARDINAL;
  names        :ARRAY [1..MAXCOUPLE] OF CoupleNameType;
  numCouple    :CARDINAL;

BEGIN

  (* Read the income information *)
  ReadCouples(names, maleIncome, femaleIncome, numCouple, MAXCOUPLE);
  (* If any data was entered compute the total income per couple and
  ** display it in a table *)
  IF(numCouple > 0) THEN
    ComputeTotals(maleIncome,femaleIncome,totalIncome,numCouple);
    DisplayTotals(names,maleIncome,femaleIncome,totalIncome,numCouple);
  ELSE
    (*No data entered *)
    WriteStringLn('No income data entered');
  END;

END Ass3.

(* Below is the test data file I used to check this program. It includes
** short names (1 char) and a 14,15,16 char name and a very long name. I've
** set the MAXNAMELEN = 15 so I checked the boundary conditions. Also there
** are some invalid income values to check Prompt2StrCard worked correctly

Addison 0 23000
Crake 34000 24000
Jones  0 0
Peters 12000 0
Smith 100000 23000
b 0 0
AVeryLongSurnameDesignedToTestTheLimits 1000000 2000000
Fourteen_Chars 10000 8000
Fifteen___Chars 12000 0
AndSixteen_Chars 23 0
ErrorTest -1 -10
ErrorAgain smith 20 jones 36
q
*)


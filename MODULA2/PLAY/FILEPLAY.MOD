MODULE Fileplay;

FROM StreamFile IMPORT Open, ChanId, OpenResults, read, opened;
FROM IOConsts IMPORT ReadResults;
FROM IOChan IMPORT Look, Skip;
FROM STextIO IMPORT WriteChar;

FROM InOut IMPORT WriteString, WriteLn;

VAR
	cid : ChanId;
  res : OpenResults;
  c : CHAR;
  read_res : ReadResults;

BEGIN
	Open(cid, 'file.txt', read, res);
	IF(res = opened) THEN
		WriteString('Yup');WriteLn;
    (*Skip(cid);*)
    Look(cid,c,read_res);
    IF(read_res = allRight) THEN
      WriteChar(c);
    ELSE
			WriteString('Nope');
		END;
    
	ELSE
		WriteString('Errr......');
	END;
	WriteLn;


END Fileplay.

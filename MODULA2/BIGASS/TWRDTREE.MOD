MODULE TWrdTree;

FROM ProgArgs IMPORT Assert;
FROM InOut IMPORT WriteString, WriteCard, WriteLn;


IMPORT WordTree;
IMPORT TextWord;


VAR
  wordtree :WordTree.WordTree;
  textword :TextWord.TextWord;

BEGIN

  WordTree.Create(wordtree);

  TextWord.Create(textword);
  TextWord.Set(textword,'Chris');

  WordTree.AddTextWord(wordtree,textword,5);
  Assert(textword = NIL,'Word not set to NIL after add');


  TextWord.Create(textword);
  TextWord.Set(textword,'Fred');
  WordTree.AddTextWord(wordtree,textword,7);

  TextWord.Create(textword);
  TextWord.Set(textword,'A');
  WordTree.AddTextWord(wordtree,textword,9);

  TextWord.Create(textword);
  TextWord.Set(textword,'A');
  WordTree.AddTextWord(wordtree,textword,9);

  TextWord.Create(textword);
  TextWord.Set(textword,'Chris');
  WordTree.AddTextWord(wordtree,textword,9);


  WordTree.Print(wordtree);

END TWrdTree.

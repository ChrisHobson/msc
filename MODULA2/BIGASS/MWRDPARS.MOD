(* ============================================================================
**  IMPLEMENTATION MODULE for WordParser
**
**  See definition module for interface details
** ----------------------------------------------------------------------------
** Chris Hobson : MSc Software Development (28001000944674)
** ----------------------------------------------------------------------------
*)
IMPLEMENTATION MODULE WordParser;

FROM ProgArgs IMPORT Assert;
FROM Storage IMPORT ALLOCATE, DEALLOCATE;
IMPORT TextWord;

(*===========================================================================*)
TYPE
  WordParser = POINTER TO WordParserData;

  WordParserData = RECORD
                         word :TextWord.TextWord;
                         hyphen :BOOLEAN;
                       END;

VAR
  numWordParser :CARDINAL;  (* Used for DebugInfo *)

(*===========================================================================*)
PROCEDURE Create(VAR ptr :WordParser);

BEGIN

  ALLOCATE(ptr,SIZE(WordParserData));
  ptr^.word := NIL;
  (* Increment the global count *)
  INC(numWordParser);

END Create;

(*===========================================================================*)
PROCEDURE Destroy(VAR ptr :WordParser);

BEGIN
  Assert(ptr <> NIL, "Destroy | NIL ptr");

  (* Just incase the parser still has a current word destroy
  ** it too *)
  IF(ptr^.word <> NIL) THEN TextWord.Destroy(ptr^.word) END;
  (* Destroy the parser *)
  DEALLOCATE(ptr,SIZE(WordParserData));
  (* Decrement the global count *)
  DEC(numWordParser);

END Destroy;

(*===========================================================================*)
PROCEDURE AddChar(ptr :WordParser; c :CHAR) :TextWord.TextWord;

VAR
  store :BOOLEAN;
  retword :TextWord.TextWord;

BEGIN
 
  store := FALSE;
  retword := NIL;
  (* Words are all uppercase *)
  c := CAP(c);

  WITH ptr^ DO
    (* If this is an alphabetic charcter *)
    IF((c >= 'A') AND (c <= 'Z')) THEN
      (* We need to store it *)
      store := TRUE;
      (* If we have no current word then create a new one *)
      IF(word = NIL) THEN
        TextWord.Create(word);
        (* Flag no hyphen found in current word *)
        hyphen := FALSE;
      END;
    (* Not an alphabetic character but we have a current word *)
    ELSIF(word <> NIL) THEN
      IF((c = "'") OR (c = '-')) THEN
        store := TRUE;
        (* Flag a hyphen has been found *)
        IF(c = '-') THEN hyphen := TRUE; END;
      (* Digits can follow hyphens i.e. Modula-2 *)
      ELSIF((hyphen = TRUE) AND ((c >= '0') AND (c <= '9'))) THEN
        store := TRUE;
      END;
    END;

    IF(store = TRUE) THEN
      TextWord.Append(word,c);
    ELSE
      (* Remove any existing word *)
      retword := word;
      word := NIL;
    END;
  END;

  RETURN retword;

END AddChar;

(*===========================================================================*)
PROCEDURE DebugInfo(VAR num :CARDINAL);

BEGIN

  num := numWordParser;

END DebugInfo;

(*===========================================================================*)
BEGIN

  (* This main module is executed automatically at program startup. 
  ** It just initialises the count varible to 0
  *)
  numWordParser := 0;


END WordParser.

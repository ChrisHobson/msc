MODULE TWrdPars;

FROM Ascii IMPORT nul;
FROM InOut IMPORT WriteLn,WriteString, Write;
IMPORT WordParser;
IMPORT TextWord;
FROM ProgArgs IMPORT Assert;

VAR
  parser :WordParser.WordParser;
  word :TextWord.TextWord;
  string :ARRAY[0..100] OF CHAR;
  count :CARDINAL;
  c :CHAR;
  numWord :CARDINAL;

BEGIN

  string := "a line; with words it's ace and ... === various-bits...";

  WordParser.Create(parser);
  count := 0;

  REPEAT
    c := string[count];
    INC(count);
    word := WordParser.AddChar(parser,c);
    IF(word <> NIL) THEN
      WriteString('::');
      TextWord.Print(word,TextWord.Length(word));
      WriteString('::');
      WriteLn;
      TextWord.Destroy(word);
    END;
  UNTIL (c = nul);

  TextWord.DebugInfo(numWord);
  Assert(numWord = 0,"Words remain");

END TWrdPars.
